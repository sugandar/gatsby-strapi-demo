import React from "react";
import { graphql } from "gatsby";
import Img from "gatsby-image";

function BlogDetails({ data }) {
  const { title, description, image } = data.strapiBlogs;
  return (
    <div>
      <h1>Blog Details Page</h1>
      <div>
        <h3>{title}</h3>
        <Img fixed={image.childImageSharp.fixed} />
        <p>{description}</p>
      </div>
    </div>
  );
}

export const query = graphql`
  query MyQuery($slug: String!) {
    strapiBlogs(slug: { eq: $slug }) {
      title
      description
      image {
        childImageSharp {
          fixed {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  }
`;

export default BlogDetails;
