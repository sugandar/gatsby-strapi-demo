import * as React from "react";
import PropTypes from "prop-types";
import { Link } from "gatsby";
import Menu from "./menu";

const Header = () => (
  <header
    style={{
      background: `rebeccapurple`,
      marginBottom: `1.45rem`,
      padding: `1.45rem 1.0875rem`,
    }}
  >
    <div className="nav-container">
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          Logo
        </Link>
      </h1>
      <nav>
        <Menu />
      </nav>
    </div>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
