import React from "react";
import { Link, graphql, useStaticQuery } from "gatsby";

export default function Menu() {
  const data = useStaticQuery(query);
  const FilteredNavList = data.NavPages.edges.filter(
    (nav) => nav.node.slug !== ""
  );
  return (
    <div>
      <ul className="menu-item-container">
        {FilteredNavList.map((nav) => {
          return (
            <li>
              <Link to={`/${nav.node.slug}`}>{nav.node.title}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

const query = graphql`
  query Menu {
    NavPages: allStrapiPages {
      edges {
        node {
          title
          slug
        }
      }
    }
  }
`;
