import React from "react";
import Layout from "../components/layout";

export default function OurPeople() {
  return (
    <Layout>
      <div>
        <h1> Our People </h1>
      </div>
    </Layout>
  );
}

// query {
//   pages{
//     title
//     sharedcomponent{
//     	... on ComponentPeoplePeople{
//         id,
//         title
//         additionalpeopledetails{
//           id,
//           title

//         }
//       }
//     }
//   }
// }
