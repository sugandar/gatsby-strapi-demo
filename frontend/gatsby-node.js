exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(
    `
      query MyQuery {
        Blogs: allStrapiBlogs {
          edges {
            node {
              slug
              id
            }
          }
        }
      }
    `
  );
  if (result.errors) {
    throw result.errors;
  }

  // Create blog articles pages.
  const articles = result.data.Blogs.edges;

  const ArticleTemplate = require.resolve("./src/templates/Blog-Details.js");

  articles.forEach((article, index) => {
    createPage({
      path: `/blog/${article.node.slug}`,
      component: ArticleTemplate,
      context: {
        slug: article.node.slug,
      },
    });
  });
};
